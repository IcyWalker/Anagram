﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using Random = System.Random;

public class AnagramGame : IAnagramGame
{
    private const string filename = "/wordlist.txt";
    private const string wordListLocation = "https://code-test-resources.s3.eu-west-2.amazonaws.com/wordlist.txt";
    private const string vowelList = "aeiou";
    private const string consonantList = "bcdfghjklmnpqrstvwxyz";

    private List<string> allWords;
    private List<string> usedWords;
    private List<int> usedWordsScore;

    private readonly string pathPersistent;
    private readonly int amountOfLettersInBaseWord;

    private Random randomiser = new Random(Guid.NewGuid().GetHashCode());
    private CancellationTokenSource cts;

    public AnagramGame()
    {
        allWords = new List<string>();
        usedWords = new List<string>();
        usedWordsScore = new List<int>();

        amountOfLettersInBaseWord = UnityEngine.Random.Range(15, 25);
        pathPersistent = Application.persistentDataPath + filename;

        cts = new CancellationTokenSource();

        try
        {
            var checkIfFileExists = CheckFileExists(pathPersistent);
            checkIfFileExists.Wait(cts.Token);

            if (!checkIfFileExists.Result)
            {
                UpdateFileFromServer(pathPersistent);
                return;
            }

            UpdateList();
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            throw;
        }
    }

    private async Task<bool> CheckFileExists(string location)
    {
#if IOS || UNITY_ANDROID
        var request = UnityWebRequest.Get(location);
        var sendWebRequest = request.SendWebRequest();
        while (!sendWebRequest.isDone)
        {
            Task.Delay(0);
        }

        return (!request.isNetworkError && !request.isHttpError);

#else
        return File.Exists(location);
#endif
    }

    private async Task UpdateFileFromServer(string location)
    {
        var request = UnityWebRequest.Get(wordListLocation);
        var sendWebRequest = request.SendWebRequest();
        while (!sendWebRequest.isDone)
        {
            await Task.Delay(0);
        }

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError($"[AnagramGame] {wordListLocation} :: Error :: {request.error}");
            return;
        }

        string wordList = request.downloadHandler.text;
        File.WriteAllText(location, wordList);

        UpdateList();
    }

    private void UpdateList()
    {
        try
        {
            allWords.Clear();
            using (StreamReader r = new StreamReader(pathPersistent))
            {
                while (!r.EndOfStream)
                {
                    allWords.Add(r.ReadLine());
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError("[AnagramGame] The file could not be read");
            Debug.LogException(e);
        }

        ShuffleBase();
    }

    private void ShuffleBase()
    {
        baseWord = "";

        var vowelsNeeded = Mathf.FloorToInt((float)amountOfLettersInBaseWord / 5);
        var constonantsNeeded = amountOfLettersInBaseWord - vowelsNeeded;

        for (int i = 0; i < vowelsNeeded; i++)
        {
            int next = randomiser.Next(-1, vowelList.Length);
            if (next == vowelList.Length || next == -1)
            {
                next = 0;
            }

            baseWord += vowelList[next];
        }


        for (int i = 0; i < constonantsNeeded; i++)
        {
            int next = randomiser.Next(-1, consonantList.Length);
            if (next == consonantList.Length || next == -1)
            {
                next = 0;
            }

            baseWord += consonantList[next];
        }

        char[] shuffledBasewords = baseWord.ToCharArray();
        Shuffle(shuffledBasewords);
        baseWord = "";
        foreach (char letter in shuffledBasewords)
        {
            baseWord += letter;
        }
    }

    /// <summary>
    /// https://www.dotnetperls.com/fisher-yates-shuffle
    /// </summary>
    private void Shuffle<T>(T[] array)
    {
        int n = array.Length;
        for (int i = 0; i < n; i++)
        {
            int r = i + randomiser.Next(n - i);
            T t = array[r];
            array[r] = array[i];
            array[i] = t;
        }
    }

    public bool SubmitWord(string word)
    {
        //check against base word
        for (var i = 0; i < word.Length; i++)
        {
            if (baseWord.All(c => word.ToLower()[i] != c))
            {
                return false;
            }
        }

        //check if word has been used before
        if (usedWords.Any(s => s.ToLower() == word.ToLower()))
        {
            return false;
        }

        if (allWords.Any(s => s.ToLower() == word.ToLower()))
        {
            usedWords.Add(word.ToLower());
            usedWordsScore.Add(word.Length);

            for (var i = 0; i < word.Length; i++)
            {
                baseWord = baseWord.Replace(word[i].ToString(), string.Empty);
            }

            ReorderList();

            return true;
        }

        return false;
    }

    private void ReorderList()
    {
        if (usedWordsScore.Count <= 1)
        {
            return;
        }

        List<int> newScores = new List<int>();
        List<string> newWords = new List<string>();
        newScores.Add(usedWordsScore[0]);
        newWords.Add(usedWords[0]);

        for (int i = 1; i < usedWordsScore.Count; i++)
        {
            bool wasInserted = false;
            for (int j = 0; j < newScores.Count; j++)
            {
                if (newScores[j] < usedWordsScore[i])
                {
                    newScores.Insert(j, usedWordsScore[i]);
                    newWords.Insert(j, usedWords[i]);
                    wasInserted = true;
                    break;
                }
            }

            if (!wasInserted)
            {
                newScores.Add(usedWordsScore[i]);
                newWords.Add(usedWords[i]);
            }
        }

        usedWordsScore = newScores;
        usedWords = newWords;
    }

    public string GetWordEntryAtPosition(int position)
    {
        if (usedWords.Count - 1 >= position)
        {
            return usedWords[position];
        }

        return "";
    }

    public int GetScoreAtPosition(int position)
    {
        if (usedWordsScore.Count - 1 >= position)
        {
            return usedWordsScore[position];
        }

        return -1;
    }

    public string baseWord { get; set; }
}